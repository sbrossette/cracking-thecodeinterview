#include <iostream>
#include <iomanip>
#include <cmath>

class Point2d
{
 public:
  Point2d(double x, double y)
    : x_(x), y_(y)
  {
  }
  virtual ~Point2d() {}
  const double& x() const { return x_; }
  const double& y() const { return y_; }
  double norm() const;
  void normalize();

 private:
  double x_;
  double y_;
};

double Point2d::norm() const
{
  double result = std::sqrt(x_ * x_ + y_ * y_);
  return result;
}

Point2d computeMiddlePoint(Point2d p1, Point2d p2)
{
  return Point2d((p1.x() + p2.x()) / 2., (p1.y() + p2.y()) / 2.);
}

double computeDistance(const Point2d& p1, const Point2d p2)
{
  double result;
  double x = p2.x() - p1.x();
  double y = p2.y() - p1.y();

  result = std::sqrt(x * x + y * y);
  return result;
}

void Point2d::normalize()
{
  double n = norm();
  if (n > 0)
  {
    x_ = x_ / n;
    y_ = y_ / n;
  }
  else
    throw "Cannot normalize a null point";
}

double computePiApprox(int numberOfDivision)
{
  Point2d p1(0, 1);
  Point2d p2(1, 0);
  for (size_t i = 0; i < numberOfDivision; i++)
  {
    Point2d newP2 = computeMiddlePoint(p1, p2);
    newP2.normalize();
    p2 = newP2;
  }
  double lengthSegment = computeDistance(p1, p2);
  double piApproximation = lengthSegment * std::pow(2, numberOfDivision + 1);
  return piApproximation;
}

int main(int argc, char* argv[])
{
  int numberOfDivision = 5;
  size_t prec = 20;
  if (argc >= 2)
  {
    numberOfDivision = std::atoi(argv[1]);
  }
  if (argc >= 3)
  {
    prec = std::atoi(argv[2]);
  }
  std::cout << std::setprecision(prec) << computePiApprox(numberOfDivision) << std::endl;
  return 0;
}
