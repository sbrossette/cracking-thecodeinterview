#include <iostream>

void foo() 
{ 
  throw "Whatever";
  throw "Whatever2";
}

int main(int argc, char* argv[])
{
  try
  {
    foo();
  }
  catch (const char* e)
  {
    std::cerr << e << std::endl;
  }
  return 0;
}
