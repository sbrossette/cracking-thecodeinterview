#include <iostream>
#include <string>

bool isRotation(const std::string& s1, const std::string& s2)
{
  std::string s1s1;
  s1s1 = s1 + s1;
  size_t i = s1s1.find(s2);
  return i != -1;
}

int main(void)
{
  
  std::string s1("HelloWorld");
  std::string s2("WorldHello");
  std::string s3("WoarldHello");
  std::cout << isRotation(s1, s2) << std::endl;
  std::cout << isRotation(s1, s3) << std::endl;
  return 0;
}
