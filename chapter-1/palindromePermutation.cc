#include <iostream>
#include <string>
#include <array>

size_t letterIndex(const char& letter)
{
  return size_t(letter) - 97;
}

std::array<bool, 26> bitVector(const std::string& s)
{
  std::array<bool, 26> res;
  for (size_t i = 0; i < res.size(); i++) 
    res[i] = false;

  for (size_t i = 0; i < s.size(); i++) 
  {
    if (s[i] != ' ') res[letterIndex(s[i])] = !res[letterIndex(s[i])];
  }

  return res;
}

bool isPalindromePermutation(const std::string& s)
{
  std::cout << "isPalindromePermutation("<<s<<"): " << std::endl;
  std::array<bool, 26> bVec(bitVector(s));
  int nOdd = 0;
  for (size_t i = 0; i < bVec.size(); i++) 
    if(bVec[i]) nOdd++;
  bool res = nOdd <= 1;
  std::cout << "res: " << res << std::endl;
  return res;
}



int main(void)
{
  std::cout << "a: " << letterIndex('a') << std::endl;
  std::cout << "b: " << letterIndex('b') << std::endl;
  std::cout << "c: " << letterIndex('c') << std::endl;
  std::cout << "d: " << letterIndex('d') << std::endl;
  std::cout << "e: " << letterIndex('e') << std::endl;
  std::cout << "f: " << letterIndex('f') << std::endl;
  std::cout << "g: " << letterIndex('g') << std::endl;
  std::cout << "h: " << letterIndex('h') << std::endl;
  std::cout << "i: " << letterIndex('i') << std::endl;
  std::cout << "j: " << letterIndex('j') << std::endl;
  std::cout << "k: " << letterIndex('k') << std::endl;
  std::cout << "l: " << letterIndex('l') << std::endl;
  std::cout << "m: " << letterIndex('m') << std::endl;
  std::cout << "n: " << letterIndex('n') << std::endl;
  std::cout << "o: " << letterIndex('o') << std::endl;
  std::cout << "p: " << letterIndex('p') << std::endl;
  std::cout << "q: " << letterIndex('q') << std::endl;
  std::cout << "r: " << letterIndex('r') << std::endl;
  std::cout << "s: " << letterIndex('s') << std::endl;
  std::cout << "t: " << letterIndex('t') << std::endl;
  std::cout << "u: " << letterIndex('u') << std::endl;
  std::cout << "v: " << letterIndex('v') << std::endl;
  std::cout << "w: " << letterIndex('w') << std::endl;
  std::cout << "x: " << letterIndex('x') << std::endl;
  std::cout << "y: " << letterIndex('y') << std::endl;
  std::cout << "z: " << letterIndex('z') << std::endl;
  isPalindromePermutation("qqqbbbqqq");
  isPalindromePermutation("qwertyuio");
  isPalindromePermutation("aabbccddeeffgghh");
  isPalindromePermutation("aabbccdddeeffgghh");
  isPalindromePermutation("aabbcccdddeeffgghh");

  return 0;
}
