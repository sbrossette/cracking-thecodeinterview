#include <iostream>
#include <string>


bool checkPermutation(const std::string& s1, const std::string& s2)
{
  if(s1.size() != s2.size())
    return false;

  int* char_count = new int[128];
  for (int i = 0; i < 128; i++) 
  {
    char_count[i] = 0;
  }
  for (size_t i = 0; i < s1.size(); i++) 
  {
    char_count[s1.at(i)] ++;
  }
  for (size_t i = 0; i < s2.size(); i++) 
  {
    if (char_count[s2.at(i)] > 0) char_count[s2.at(i)]--;
    else return false;
  }
  return true;
}

int main(void)
{
  std::string s1("string1");
  std::string s2("string2");
  std::string s3("tgsri2n");
  std::string s4("aaaabb");
  std::string s5("abaaba");
  std::cout << "checkPermutation(s1,s2): " << checkPermutation(s1,s2) << std::endl;
  std::cout << "checkPermutation(s1,s1): " << checkPermutation(s1,s1) << std::endl;
  std::cout << "checkPermutation(s1,s3): " << checkPermutation(s2,s3) << std::endl;
  std::cout << "checkPermutation(s4,s5): " << checkPermutation(s4,s5) << std::endl;

  return 0;
}
