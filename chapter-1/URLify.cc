#include <iostream>
#include <string>

void urlify(std::string& s, size_t l)
{
  size_t index = s.size() - 1;
  for (size_t i = l; i > 0; i--)
  {
    if(s[i-1] != ' ')
    {
      s[index - 1] = s[i - 1];
      index--;
    }
    else
    {
      s[index-1] = '0';
      s[index-2] = '2';
      s[index-3] = '%';
      index-=3;
    }
  }
}

int main(void)
{
  std::string s1("Hello World I am john!!         ");
  std::cout << "s1: " << s1 << std::endl;
  urlify(s1, 23);
  std::cout << "s1: " << s1 << std::endl;
  return 0;
}
