#include <iostream>
#include <unordered_map>

bool isUnique(const std::string& s)
{
  if(s.size()>=128)
    return false;
  if(s.size()<=1)
    return true;
  
  std::array<bool, 128> char_set;
  for (size_t i = 0; i < char_set.size(); i++) 
  {
    char_set[i] = 0;
  }
  for (size_t i = 0; i < s.size(); i++) 
  {
    size_t iChar = s[i];
    if(char_set[iChar] == false)
      char_set[iChar] = true;
    else
      return false;
  }
  return true;
}

int main(void)
{
  std::string notUnique("Hello");
  std::string unique("qwetryuio");
  std::cout << "isUnique(notUnique): " << isUnique(notUnique) << std::endl;
  std::cout << "isUnique(unique): " << isUnique(unique) << std::endl;
  return 0;
}
