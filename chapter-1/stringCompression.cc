#include <iostream>
#include <string>

std::string compress(const std::string& s)
{
  std::string compressedStr("");
  int nc = 1;
  char c = s[0];
  for (size_t i = 0; i < s.size(); i++) 
  {
    if(i+1 == s.size() || s[i+1] != c)
    {
      compressedStr += c + std::to_string(nc);
      nc = 1;
      if (i + 1 != s.size()) c = s[i + 1];
    }
    else
      nc++;
  }
  std::cout << s << ": " << compressedStr << std::endl;
  return compressedStr;
}

int main(void)
{
  std::string s1("helloworld");
  std::string s2("qqqqqq");
  std::string s3("aaabbcaabbaa");
  compress(s1);
  compress(s2);
  compress(s3);
  return 0;
}
