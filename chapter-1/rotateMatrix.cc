#include <iostream>
#include <Eigen/Core>

void swap(Eigen::MatrixXd& m, long i, long j)
{
  long n = m.rows()-1;
  double temp = m(i,j);
  m(i,j) = m(n-j, i);
  m(n-j, i) = m(n-i, n-j);
  m(n-i, n-j) = m(j, n-i);
  m(j, n-i) = temp;
}
void rotate(Eigen::MatrixXd& m)
{
  std::cout << "m: \n" << m << std::endl;
  long n = m.rows() ;
  for (long i = 0; i < n/2; i++) 
  {
    for(long j = i; j < n-i-1; j++)
    {
      swap(m, i, j);
    }
  }
  std::cout << "rot m: \n" << m << std::endl;
}

int main(void)
{
  Eigen::MatrixXd m2(2,2);
  m2 << 1, 2, 3, 4;
  rotate(m2);

  Eigen::MatrixXd m3(3,3);
  m3 << 1, 2, 3, 4, 5, 6, 7, 8, 9;
  rotate(m3);

  Eigen::MatrixXd m4(4,4);
  m4 << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16;
  rotate(m4);

  Eigen::MatrixXd m5(5,5);
  m5 << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25;
  rotate(m5);
  rotate(m5);
  rotate(m5);
  rotate(m5);

  return 0;
}
