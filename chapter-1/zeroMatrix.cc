#include <iostream>
#include <Eigen/Core>

void zeroMatrix(Eigen::MatrixXi& m)
{
  std::vector<bool> rowIsSetZero(m.rows());
  std::vector<bool> colIsSetZero(m.cols());
  for (size_t i = 0; i < m.rows(); i++) 
    rowIsSetZero[i] = false;
  for (size_t i = 0; i < m.cols(); i++) 
    colIsSetZero[i] = false;
  for (int i = 0; i < m.rows(); i++)
  {
    if (!rowIsSetZero[i])
    {
      for (int j = 0; j < m.cols(); j++)
      {
        if (!colIsSetZero[j] && !rowIsSetZero[i])
        {
          if(m(i,j) == 0)
          {
            rowIsSetZero[i] = true;
            colIsSetZero[j] = true;
            std::cout << "set row " << i << " to zero" << std::endl;
            std::cout << "set col " << j << " to zero" << std::endl;
            m.row(i).setZero();
            m.col(j).setZero();
          }
        }
      }
    }
  }
}

int main(void)
{
  Eigen::MatrixXi m(4,5);
  m.setRandom();
  m(2, 3) = 0;
  m(1, 1) = 0;
  std::cout << "m: " << m << std::endl;
  zeroMatrix(m);
  std::cout << "m: " << m << std::endl;

  return 0;
}
