#include "../data-structures/Stack.hh"

template <typename T>
class StackMin : public Stack<T>
{
 public:
  StackMin() {}

  StackMin(const T& item) : Stack<T>(item) { min_ = new StackNode<T>(item); }

  void push(const T& item)
  {
    Stack<T>::push(item);
    if (min_ == nullptr)
    {
      min_ = new StackNode<T>(item);
      return;
    }

    if (item <= min_->data_)
    {
      StackNode<T>* newMin = new StackNode<T>(item);
      newMin->next_ = min_;
      min_ = newMin;
    }
  }

  void pop()
  {
    if (Stack<T>::peek().data_ == min_->data_) min_ = min_->next_;
    Stack<T>::pop();
  }

  StackNode<T> min() { return *min_; }

 public:
  StackNode<T>* min_ = nullptr;
};

