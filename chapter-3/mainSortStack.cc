#include "sortStack.hh"

int main(void)
{
  Stack<int> s;
  sortStack(s);
  s.push(1);
  sortStack(s);
  s.push(4);
  s.push(3);
  s.push(6);
  s.push(0);
  s.push(3);
  s.push(5);
  s.push(2);
  s.push(5);
  s.push(7);
  s.push(8);
  s.print();
  std::cout << "sort" << std::endl;
  sortStack(s);
  s.print();
  return 0;
}
