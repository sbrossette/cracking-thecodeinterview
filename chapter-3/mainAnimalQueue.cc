#include "AnimalQueue.hh"

int main(void)
{
  AnimalQueue shelter;
  shelter.enqueue(DOG, "d0");
  shelter.enqueue(DOG, "d1");
  shelter.enqueue(DOG, "d2");
  shelter.enqueue(CAT, "c0");
  shelter.enqueue(CAT, "c1");
  shelter.enqueue(DOG, "d4");
  shelter.enqueue(CAT, "c2");
  shelter.dequeueAny();
  shelter.dequeueCat();
  shelter.dequeueDog();
  shelter.dequeueCat();
  shelter.dequeueAny();
  shelter.dequeueAny();
  shelter.dequeueAny();
  shelter.dequeueAny();
  shelter.dequeueAny();
  shelter.dequeueAny();
  shelter.dequeueAny();
  return 0;
}
