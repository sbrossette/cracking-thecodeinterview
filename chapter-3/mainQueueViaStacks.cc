#include "QueueViaStacks.hh"

int main(void)
{
  QueueViaStacks<int> q;
  q.add(0);
  q.add(1);
  q.add(2);
  q.add(3);
  q.add(4);
  q.add(5);
  q.add(6);
  std::cout << q.dequeue() << std::endl;
  std::cout << q.dequeue() << std::endl;
  std::cout << q.peek() << std::endl;
  std::cout << q.dequeue() << std::endl;
  std::cout << q.dequeue() << std::endl;
  std::cout << q.dequeue() << std::endl;
  q.add(7);
  q.add(8);
  std::cout << q.dequeue() << std::endl;
  std::cout << q.dequeue() << std::endl;
  std::cout << q.dequeue() << std::endl;
  std::cout << q.dequeue() << std::endl;
  return 0;
}
