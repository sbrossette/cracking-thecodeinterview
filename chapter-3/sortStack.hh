#include "../data-structures/Stack.hh"

template<typename T>
void sortStack(Stack<T>& s)
{
  if(s.isEmpty())
    return;

  Stack<T> s2(s.pop());
  while(!s.isEmpty())
  {
    T tmp = s.pop();
    while(!s2.isEmpty() && s2.peek()>tmp)
    {
      s.push(s2.pop());
    }
    s2.push(tmp);
  }
  while(!s2.isEmpty())
  {
    s.push(s2.pop());
  }
}
