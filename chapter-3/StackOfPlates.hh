#include "../data-structures/Stack.hh"
#include "../data-structures/ArrayList.hh"

template<typename T>
class SetOfStacks
{
  public:
   SetOfStacks(int cap) : capacity_(cap) {}
    virtual ~SetOfStacks() {}
    void push(const T& item)
    {
      if(stacks_.size() == 0 || stacksHeight_.getLast() == capacity_)
      {
        stacks_.push_back(Stack<T>(item));
        stacksHeight_.push_back(1);
      }
      else
      {
        stacks_.getLast().push(item);
        stacksHeight_.getLast()++;
      }
    }
    void pop()
    {
      if(stacksHeight_.getLast() == 0)
      {
        stacks_.removeLast();
        stacksHeight_.removeLast();
      }

      stacks_.getLast().pop();
      stacksHeight_.getLast()--;
    }
    void popAt(int i)
    {
      if(stacksHeight_.get(i) == 0)
        throw "can't popAt on empty stack";
      else
      {
        stacks_.get(i).pop();
        stacksHeight_.get(i)--;
      }
    }
    void print()
    {
      for (size_t i = 0; i < stacks_.size(); i++) 
      {
        std::cout << "stack " << i << " height: " << stacksHeight_.get(i) << std::endl;
        stacks_.get(i).print();
      }
    }
  public:
    ArrayList<Stack<T>> stacks_;
    size_t capacity_;
    ArrayList<int> stacksHeight_;
};
