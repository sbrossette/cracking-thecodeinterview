#include <iostream>
#include <list>

class Animal
{
 public:
  Animal(const std::string& name, const int& order) : name_(name), order_(order)
  {
  }
  virtual ~Animal() {}
  const int& order() const { return order_; }
  void setOrder(int o) { order_ = o; }
  const std::string& name() const { return name_; }
  virtual std::string noise() = 0;

 protected:
  int order_;
  std::string name_;
};

class Cat : public Animal
{
 public:
  Cat(const std::string& name, const int& order) : Animal(name, order) {}
  virtual ~Cat() {}
  std::string noise() { return std::string("Meow");}

 private:
};

class Dog : public Animal
{
 public:
  Dog(const std::string& name, const int& order) : Animal(name, order) {}
  virtual ~Dog() {}
  std::string noise() { return std::string("Woof");}

 private:
};

enum Pet {DOG, CAT};

class AnimalQueue
{
 public:
  AnimalQueue(){}
  virtual ~AnimalQueue(){}
  void enqueue(Pet type, std::string name)
  {
    order_++;
    if(type == DOG)
    {
      dogs_.push_back(Dog(name, order_));
      std::cout << "Shelter received dog named " << name << std::endl;
    }
    else if (type == CAT)
    {
      cats_.push_back(Cat(name, order_));
      std::cout << "Shelter received cat named " << name << std::endl;
    }
  }

  void dequeueCat()
  {
    if (cats_.empty()) throw "No cats in the shelter";
    std::cout << "Someone adopted a cat named: " << cats_.front().name()
              << std::endl;
    return cats_.pop_front();
  }
  void dequeueDog()
  {
    if (dogs_.empty()) throw "No dogs in the shelter";
    std::cout << "Someone adopted a dog named: " << dogs_.front().name() << std::endl;
    return dogs_.pop_front();
  }

  void dequeueAny()
  {
    if(cats_.empty() && dogs_.empty())
      throw "No aminals in the shelter";
    if (dogs_.empty() || (cats_.front().order() < dogs_.front().order()))
    {
      dequeueCat();
    }
    else
    {
      dequeueDog();
    }
  }

 private:
  std::list<Cat> cats_;
  std::list<Dog> dogs_;
  int order_ = 0;

  /* data */
};
