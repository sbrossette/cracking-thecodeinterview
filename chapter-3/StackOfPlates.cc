#include <iostream>
#include "StackOfPlates.hh"

int main(void)
{
  SetOfStacks<int> stacks(4);
  stacks.push(8);
  stacks.push(1);
  stacks.push(2);
  stacks.push(2);
  stacks.push(3);
  stacks.push(5);
  stacks.push(3);
  stacks.push(2);
  stacks.push(2);
  stacks.push(2);
  stacks.push(3);
  stacks.push(5);
  stacks.push(3);
  stacks.push(2);
  stacks.push(2);
  stacks.push(3);
  stacks.push(5);
  stacks.print();
  stacks.popAt(3);
  stacks.popAt(3);
  stacks.print();
  stacks.pop();
  stacks.print();
  stacks.pop();
  stacks.print();
  stacks.pop();
  stacks.print();
  stacks.pop();
  stacks.print();
  return 0;
}

