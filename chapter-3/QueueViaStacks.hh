#include <iostream>
#include "../data-structures/Stack.hh"

template<typename T>
class QueueViaStacks
{
public:
  QueueViaStacks (){};
  virtual ~QueueViaStacks (){};
  void add(const T& item)
  {
    newest_.push(item);
  }
  T dequeue()
  {
    shiftStack();
    return oldest_.pop();
  }
  T peek()
  {
    shiftStack();
    return oldest_.peek();
  }
  void shiftStack()
  {
    if(oldest_.isEmpty())
      while(!newest_.isEmpty())
        oldest_.push(newest_.pop());
  }

  

private:
  Stack<T> oldest_;
  Stack<T> newest_;
};
