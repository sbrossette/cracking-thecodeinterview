#include "StackMin.hh"

int main(void)
{
  StackMin<int> s;
  s.push(10);
  s.push(100);
  s.push(10);
  s.push(4);
  s.push(20);
  s.push(12);
  s.push(3);
  s.push(6);
  s.push(3);
  s.push(3);
  s.push(5);
  s.push(3);
  std::cout << "s" << std::endl;
  s.head_->print();
  std::cout << "sMin" << std::endl;
  s.min_->print();

  std::cout << std::endl;
  std::cout << std::endl;

  s.pop();
  s.pop();
  s.pop();
  s.pop();
  s.pop();
  s.pop();
  std::cout << "s" << std::endl;
  s.head_->print();
  std::cout << "sMin" << std::endl;
  s.min_->print();
  return 0;
}
