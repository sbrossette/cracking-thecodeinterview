#include <iostream>
#include <vector>
#include "buildOrder.hh"

int main(void)
{
  Project* a = new Project("a");
  Project* b = new Project("b");
  Project* c = new Project("c");
  Project* d = new Project("d");
  Project* e = new Project("e");
  Project* f = new Project("f");
  Project* g = new Project("g");
  Project* h = new Project("h");
  std::vector<Project*> projects = {a, b, c, d, e, f, g, h};

  std::vector<std::pair<Project*, std::string>> depList = 
  {
    {d, "a"},
    {b, "f"},
    {d, "b"},
    {a, "f"},
    {c, "d"},
  };

  auto res = buildOrder(projects, depList);
  std::cout << "Build Order:" << std::endl;
  for (auto i : res)
  {
    std::cout << i->name() << std::endl;
  }
  return 0;
}
