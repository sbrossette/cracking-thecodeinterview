#pragma once
#include <iostream>

class NodeBT
{
 public:
  NodeBT(const int& item) : data_(item) {}
  ~NodeBT() {}

 public:
  int data_;
  NodeBT* left_ = nullptr;
  NodeBT* right_ = nullptr;
};

class DepthAndBalance
{
 public:
  DepthAndBalance() {}
  virtual ~DepthAndBalance() {}
  DepthAndBalance(size_t d, bool b) : depth_(b), balanced_(b) {}

 public:
  size_t depth_ = 0;
  bool balanced_ = true;
};

bool isDiffLessThanOne(size_t a, size_t b)
{
  int diff = static_cast<int>(a) - static_cast<int>(b);
  return (diff <= 1 && diff >= -1);
}

DepthAndBalance checkDepthAndBalance(NodeBT* n)
{
  if (n == nullptr) return DepthAndBalance(0, true);
  DepthAndBalance res;
  DepthAndBalance resLeft = checkDepthAndBalance(n->left_);
  DepthAndBalance resRight = checkDepthAndBalance(n->right_);
  res.depth_ = std::max(resLeft.depth_, resRight.depth_) + 1;
  res.balanced_ = (resLeft.balanced_ && resRight.balanced_ &&
                   isDiffLessThanOne(resLeft.depth_, resRight.depth_));
  return res;
}

bool isBalanced(NodeBT n) { return checkDepthAndBalance(&n).balanced_; }
