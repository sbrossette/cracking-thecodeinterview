#include <vector>
#include "createMinBST.hh"
#include "listOfDepth.hh"

int main(void)
{
  std::vector<int> v = {1,  2,  3,  4,  5,  6,  7,  8, 9,
                        10, 11, 12, 13, 14, 15, 16, 17};
  NodeBST* n = createMinBST(v, 0, v.size() - 1);
  n->print();
  std::list<std::list<NodeBST>> res = listOfDepth(*n);
  for (auto depthList : res)
  {
    for (auto node : depthList)
    {
      std::cout << node.data_ << ", ";
    }
    std::cout << std::endl;
  }

  return 0;
}
