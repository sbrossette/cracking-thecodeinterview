#include <iostream>
#include <vector>

class Project
{
 public:
  Project(const std::string& name) : name_(name) {}
  virtual ~Project() {}
  bool hasDependency(const std::string& dep)
  {
    for (auto d : dependencies_)
    {
      if (!dep.compare(d))
        return true;
    }
    return false;
  }
  void addDependency(const std::string& dep)
  {
    if (!hasDependency(dep)) dependencies_.push_back(dep);
  }
  void removeDependency(const std::string& dep)
  {
    for (size_t i = 0; i < dependencies_.size(); i++)
    {
      if (!dep.compare(dependencies_[i]))
        dependencies_.erase(dependencies_.begin() + i);
    }
  }
  const std::string& name() const { return name_; }
  void print()
  {
    std::cout << "Project: " << name_ << ", depending on {";
    for (auto d : dependencies_)
    {
      std::cout << d << ", ";
    }
    std::cout << "}" << std::endl;
  }
  const std::vector<std::string>& dependencies() const { return dependencies_; }

 private:
  std::string name_;
  std::vector<std::string> dependencies_;
};

std::vector<Project*> buildOrder(
    std::vector<Project*> projects,
    std::vector<std::pair<Project*, std::string>> depList)
{
  for (auto i : depList)
  {
    i.first->addDependency(i.second);
  }
  std::cout << "Projects to build: " << std::endl;
  for (auto i : projects) i->print();
  std::vector<Project*> buildOrder;
  std::vector<Project*> remainingToBuild = projects;
  while (!remainingToBuild.empty())
  {
    //std::cout << "Remaining to build: " << std::endl;
    //for (auto i : remainingToBuild) i->print();

    std::vector<Project*> readyToBuild;
    std::vector<size_t> indexToErase;
    for (size_t i = 0; i < remainingToBuild.size(); i++)
    {
      if (remainingToBuild[i]->dependencies().empty())
      {
        //std::cout << remainingToBuild[i]->name() << " is ready to build"
                  //<< std::endl;
        readyToBuild.push_back(remainingToBuild[i]);
        buildOrder.push_back(remainingToBuild[i]);
        indexToErase.push_back(i);
      }
      //else
      //{
        //std::cout << remainingToBuild[i]->name()
                  //<< " is not ready to build, depends on ";
        //for (auto dep : remainingToBuild[i]->dependencies())
        //{
          //std::cout << dep << ",";
        //}
        //std::cout << std::endl;
      //}
    }
    for (int i = indexToErase.size() - 1; i >= 0; i--)
    {
      remainingToBuild.erase(remainingToBuild.begin() + indexToErase[i]);
    }

    if (readyToBuild.empty()) throw "Error cycle detected";

    for (size_t i = 0; i < remainingToBuild.size(); i++)
    {
      for (size_t j = 0; j < readyToBuild.size(); j++)
      {
        remainingToBuild[i]->removeDependency(readyToBuild[j]->name());
      }
    }
  }
  return buildOrder;
}
