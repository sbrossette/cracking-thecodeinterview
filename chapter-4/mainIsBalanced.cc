#include <iostream>
#include "isBalanced.hh"

int main(void)
{
  NodeBT root(0);
  std::cout << "isBalanced(root): " << isBalanced(root) << std::endl;
  root.left_ = new NodeBT(1);
  root.right_ = new NodeBT(1);
  std::cout << "isBalanced(root): " << isBalanced(root) << std::endl;
  root.left_->left_ = new NodeBT(2);
  std::cout << "isBalanced(root): " << isBalanced(root) << std::endl;
  root.left_->left_->right_ = new NodeBT(2);
  std::cout << "isBalanced(root): " << isBalanced(root) << std::endl;
  return 0;
}
