#include <vector>
#include "createMinBST.hh"

int main(void)
{
  std::vector<int> v = {1, 2, 3, 4, 5, 6 ,7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};
  NodeBST* n = createMinBST(v, 0, v.size()-1);
  n->print();
  return 0;
}
