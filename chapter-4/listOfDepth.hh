#pragma once
#include <iostream>
#include <list>
#include "createMinBST.hh"

std::list<std::list<NodeBST>> listOfDepth(NodeBST root)
{
  std::list<std::list<NodeBST>> allDepth;
  std::list<NodeBST> rootList;
  rootList.push_back(root);
  allDepth.push_back(rootList);
  bool emptyDepth = false;
  if (root.left_ == nullptr && root.right_ == nullptr) emptyDepth = true;
  while (!emptyDepth)
  {
    std::list<NodeBST> depthList;
    for (NodeBST n : allDepth.back())
    {
      if (n.left_ != nullptr) depthList.push_back(*n.left_);
      if (n.right_ != nullptr) depthList.push_back(*n.right_);
      emptyDepth = depthList.empty();
    }
    allDepth.push_back(depthList);
  }
  return allDepth;
}
