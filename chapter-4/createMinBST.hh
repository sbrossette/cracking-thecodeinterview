#pragma once
#include <iostream>
#include <vector>

class NodeBST
{
 public:
  NodeBST(const int& item) : data_(item) {}
  virtual ~NodeBST() {}
  void print()
  {
    if (left_ != nullptr) std::cout << left_->data_;
    std::cout << " < " << data_ << " < ";
    if (right_ != nullptr) std::cout << right_->data_;
    std::cout << std::endl;
    if (left_ != nullptr) left_->print();
    if (right_ != nullptr) right_->print();
  }

  int data_;
  NodeBST* left_ = nullptr;
  NodeBST* right_ = nullptr;
};

NodeBST* createMinBST(std::vector<int> a, size_t min, size_t max)
{
  if (max == min) return new NodeBST(a[min]);
  size_t center = (max + min) / 2;
  NodeBST* n = new NodeBST(a[center]);
  if (center - 1 >= min) n->left_ = createMinBST(a, min, center - 1);
  if (center + 1 <= max) n->right_ = createMinBST(a, center + 1, max);
  return n;
}
