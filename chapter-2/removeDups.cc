#include <unordered_map>
#include "../data-structures/LinkedList.hh"

void removeDups(LinkedList<int>& l)
{
  Node<int>* n = l.head();
  Node<int>* nPrev = l.head();
  std::unordered_map<int, bool> hashTable;
  hashTable.insert({n->data_, true});

  while (n->next_ != nullptr)
  {
    nPrev = n;
    n = n->next_;
    if (hashTable.find(n->data_) != hashTable.end())
    {
      nPrev->next_ = n->next_;
      n = nPrev;
    }
    else
    {
      hashTable.insert({n->data_, true});
    }
  }
}

int main(void)
{
  LinkedList<int> l(1);
  l.appendToTail(2);
  l.appendToTail(3);
  l.appendToTail(4);
  l.appendToTail(4);
  l.appendToTail(3);
  l.appendToTail(2);
  l.appendToTail(5);
  l.appendToTail(1);
  l.appendToTail(1);
  l.appendToTail(1);
  l.appendToTail(1);
  l.appendToTail(1);
  l.appendToTail(1);
  l.print();
  removeDups(l);
  l.print();
  return 0;
}
