#include <iostream>
#include "../data-structures/LinkedList.hh"

void partition(LinkedList<int>& l, int d)
{
  Node<int>* head = l.head();
  Node<int>* tail = l.head();
  Node<int>* n = l.head();
  Node<int>* nNext = l.head();
  while(n != nullptr)
  {
    nNext = n->next_;
    if(n->data_ >= d)
    {
      tail->next_ = n;
      tail = n;
    }
    else
    {
      n->next_ = head;
      head = n;
    }
    n = nNext;
  }
  tail->next_ = nullptr;
  l.setHead(head);
}

int main(void)
{
  LinkedList<int> l(11);
  l.appendToTail(2);
  l.appendToTail(3);
  l.appendToTail(4);
  l.appendToTail(4);
  l.appendToTail(3);
  l.appendToTail(2);
  l.appendToTail(5);
  l.appendToTail(7);
  l.appendToTail(2);
  l.appendToTail(5);
  l.appendToTail(13);
  l.appendToTail(2);
  l.appendToTail(1);
  l.print();
  partition(l, 5);
  l.print();
  return 0;
}
