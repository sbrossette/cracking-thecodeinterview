#include <iostream>
#include "../data-structures/LinkedList.hh"

void deleteMiddleNode(Node<int>* n)
{
  n->data_ = n->next_->data_;
  n->next_ = n->next_->next_;
}

int main(void)
{
  LinkedList<int> l(1);
  l.appendToTail(2);
  l.appendToTail(3);
  l.appendToTail(4);
  l.appendToTail(4);
  l.appendToTail(3);
  l.appendToTail(2);
  l.appendToTail(5);
  l.appendToTail(7);
  l.appendToTail(2);
  l.appendToTail(5);
  l.appendToTail(13);
  l.appendToTail(2);
  l.appendToTail(1);
  l.print();
  Node<int>* n = l.head();
  n = n->next_->next_->next_->next_->next_;
  deleteMiddleNode(n);
  l.print();
  
  return 0;
}
