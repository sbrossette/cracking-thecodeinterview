#include <iostream>
#include "../data-structures/LinkedList.hh"

Node<int>* kthToLast(LinkedList<int> l, int k)
{
  Node<int>* nEnd = l.head();
  Node<int>* nK = l.head();
  for (int i = 0; i < k; i++) 
  {
    if (nEnd->next_ != nullptr)
      nEnd = nEnd->next_;
    else
    {
      std::cout << "not enough elements" << std::endl;
      return l.head();
    }
  }
  while(nEnd->next_ != nullptr)
  {
    nEnd = nEnd->next_;
    nK = nK->next_;
  }
  return nK;
}

int main(void)
{
  LinkedList<int> l(1);
  l.appendToTail(2);
  l.appendToTail(3);
  l.appendToTail(4);
  l.appendToTail(4);
  l.appendToTail(3);
  l.appendToTail(2);
  l.appendToTail(5);
  l.appendToTail(7);
  l.appendToTail(2);
  l.appendToTail(5);
  l.appendToTail(13);
  l.appendToTail(2);
  l.appendToTail(1);
  l.print();
  Node<int>* res = kthToLast(l,0);
  std::cout << res->data_ << std::endl;
  res = kthToLast(l,1);
  std::cout << res->data_ << std::endl;
  res = kthToLast(l,2);
  std::cout << res->data_ << std::endl;
  res = kthToLast(l,3);
  std::cout << res->data_ << std::endl;
  res = kthToLast(l,4);
  std::cout << res->data_ << std::endl;
  res = kthToLast(l,40);
  std::cout << res->data_ << std::endl;
  return 0;
}
