#include <iostream>
#include "../data-structures/LinkedList.hh"

class TailAndLength
{
 public:
  TailAndLength(Node<int>* n)
  {
    if (n == nullptr)
    {
      length_ = 0;
      tail_ = nullptr;
      return;
    }
    length_ = 1;
    tail_ = n;
    while (tail_->next_ != nullptr)
    {
      tail_ = tail_->next_;
      length_++;
    }
  }

 public:
  Node<int>* tail_;
  int length_;
};

Node<int>* intersection(Node<int>* n1, Node<int>* n2)
{
  if (n1 == nullptr || n2 == nullptr) return nullptr;

  TailAndLength tl1(n1);
  TailAndLength tl2(n2);

  if (tl1.tail_ != tl2.tail_)
    return nullptr;

  Node<int>* longList = nullptr;
  Node<int>* shortList = nullptr;
  if (tl1.length_ == tl2.length_)
  {
    longList = n1;
    shortList = n2;
  }
  else if (tl1.length_ > tl2.length_)
  {
    longList = n1;
    shortList = n2;
    for (int i = 0; i < tl1.length_ - tl2.length_; i++)
      longList = longList->next_;
  }
  else
  {
    longList = n2;
    shortList = n1;
    for (int i = 0; i < tl2.length_ - tl1.length_; i++)
      longList = longList->next_;
  }

  while (longList != shortList)
  {
    longList = longList->next_;
    shortList = shortList->next_;
  }
  return longList;
}

int main(void)
{
  Node<int> n1(1);
  Node<int> n2(2);
  Node<int> n3(3);
  Node<int> n4(4);
  Node<int> n5(5);
  Node<int> n6(6);
  Node<int> n7(7);
  Node<int> n8(8);
  Node<int> n9(9);
  Node<int> m1(10);
  Node<int> m2(20);
  Node<int> m3(30);
  Node<int> m4(40);
  Node<int> m5(50);
  Node<int> m6(60);

  n1.next_ = &n2;
  n2.next_ = &n3;
  n3.next_ = &n4;
  n4.next_ = &n5;
  n5.next_ = &n6;
  n6.next_ = &n7;
  n7.next_ = &n8;
  n8.next_ = &n9;

  m1.next_ = &m2;
  m2.next_ = &m3;
  m3.next_ = &n3;
  m4.next_ = &m5;
  m5.next_ = &m6;
  m6.next_ = &n2;

  n1.print();
  m1.print();

  Node<int>* nInt = intersection(&n1, &m1);
  nInt->print();

  return 0;
}
