#include <iostream>
#include "../data-structures/LinkedList.hh"

Node<int>* sumLists(Node<int>* l1, Node<int>* l2, int carry = 0)
{
  Node<int>* res = new Node<int>(0);

  int value = carry;
  if (l1 != nullptr) value += l1->data_;
  if (l2 != nullptr) value += l2->data_;

  res->data_ = value % 10;

  if (l1 != nullptr || l2 != nullptr)
  {
    Node<int>* more =
        sumLists(l1 == nullptr ? nullptr : l1->next_,
                 l2 == nullptr ? nullptr : l2->next_, value >= 10 ? 1 : 0);

    res->next_ = more;
  }

  return res;
}

int main(void)
{
  Node<int> l1(7);
  l1.appendToTail(1);
  l1.appendToTail(6);
  Node<int> l2(5);
  l2.appendToTail(9);
  l2.appendToTail(2);

  l1.print();
  l2.print();
  Node<int>* l3 = sumLists(&l1, &l2);
  l3->print();

  return 0;
}
