#include <iostream>
#include <stack>

#include "../data-structures/LinkedList.hh"

bool isPalindrome(Node<int>* n)
{
  if (n->next_ == nullptr || n == nullptr)
    return true;
  std::stack<int> s; 
  Node<int>* nSlow = n;
  Node<int>* nFast = n;
  while(nFast->next_ != nullptr && nFast->next_ != nullptr)
  {
    std::cout << "Adding " << nSlow->data_ << std::endl;
    s.push(nSlow->data_);
    nSlow = nSlow->next_;
    nFast = nFast->next_->next_;
  }

  return true;
}

int main(void)
{

  Node<int> l1(7);
  l1.appendToTail(1);
  l1.appendToTail(6);
  l1.appendToTail(1);
  l1.appendToTail(1);
  l1.appendToTail(6);
  isPalindrome(&l1);


  return 0;
}

