#include <iostream>
#include "../data-structures/LinkedList.hh"

Node<int>* sumLists(Node<int>* l1, Node<int>* l2)
{
  l1->print();
  l2->print();
  if(l1 ==nullptr)
    return l2;
  if(l2==nullptr)
    return l1;

  Node<int>* res = new Node<int>(0);
  int dozens = 0;
  bool firstDigit = true;
  while(!(l1 == nullptr && l2 == nullptr && dozens == 0))
  {
    int l1data = 0;
    int l2data = 0;
    if(l1 != nullptr)
      l1data = l1->data_;
    if(l2 != nullptr)
      l2data = l2->data_;

    int sumDigits = l1data + l2data + dozens;
    int unitPart = sumDigits % 10;
    dozens = (sumDigits - unitPart)/10;
    if(firstDigit)
    {
      res->data_ = unitPart;
      firstDigit = false;
    }
    else
      res->appendToTail(unitPart);

    if(l1 != nullptr)
      l1 = l1->next_;
    if(l2 != nullptr)
      l2 = l2->next_;
  }
  return res;
}

int main(void)
{
  Node<int> l1(7);
  l1.appendToTail(1);
  l1.appendToTail(6);
  Node<int> l2(5);
  l2.appendToTail(9);
  l2.appendToTail(2);

  Node<int>* l3 = sumLists(&l1, &l2);
  l3->print();

  
  return 0;
}
