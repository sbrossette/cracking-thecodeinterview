
#include <iostream>
#include <vector>
#include <queue>

class GraphNode
{
  public:
    GraphNode(double data);
    virtual ~GraphNode();
    void addChild(double data);
    double data() const {return data_;}
    const std::vector<GraphNode*>& children() { return children_;}
    GraphNode* getChild(size_t index);
  private:
    double data_;
    std::vector<GraphNode*> children_;
};

GraphNode::GraphNode(double data)
  : data_(data)
{
  std::cout << "ctor GraphNode(" << data_ << ")" << std::endl;
}

GraphNode::~GraphNode()
{
  std::cout << "dtor GraphNode(" << data_ << ")" << std::endl;
  for(size_t i = 0; i < children_.size(); ++i)
    delete children_[i];
}

void GraphNode::addChild(double data)
{
  children_.push_back(new GraphNode(data));
}

GraphNode* GraphNode::getChild(size_t index)
{
  if(index >= children_.size())
    throw "index out of bound in getChild";
  else
    return children_[index];
}

void depthFirstTraversal(GraphNode* root)
{
  std::cout << root->data() << std::endl;
  for(size_t i = 0; i<root->children().size(); ++i)
  {
    depthFirstTraversal(root->getChild(i));
  }
}

void breadthFirstTraversal(GraphNode* root)
{
  std::queue<GraphNode*> nodeQueue;
  nodeQueue.push(root);
  while(!nodeQueue.empty())
  {
    GraphNode* node = nodeQueue.front();
    nodeQueue.pop();
    std::cout << node->data() << std::endl;
    for(size_t i = 0; i < node->children().size(); ++i)
    {
      nodeQueue.push(node->getChild(i));
    }
  }
}

int main()
{
  try
  {
    GraphNode root(0);
    root.addChild(1);
    root.addChild(2);
    root.addChild(3);
    root.addChild(4);
    root.addChild(5);
    GraphNode* c1 = root.getChild(0);
    GraphNode* c2 = root.getChild(1);
    GraphNode* c4 = root.getChild(3);
    GraphNode* c5 = root.getChild(4);
    c2->addChild(21);
    c2->addChild(22);
    c4->addChild(41);
    c4->addChild(42);
    c4->addChild(43);
    c4->addChild(44);
    c4->addChild(45);
    c4->addChild(46);
    GraphNode* c46 = c4->getChild(5);
    c46->addChild(461);
    std::cout << "depth-first:"<< std::endl;
    depthFirstTraversal(&root);
    std::cout << "breadth-first:"<< std::endl;
    breadthFirstTraversal(&root);
  }
  catch ( const char * e)
  {
    std::cerr << e << std::endl;
  }
  return 0;
}




