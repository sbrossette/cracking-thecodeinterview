
#include <iostream>

class DynArray
{
  public:
    DynArray(size_t capacity=1);
    virtual ~DynArray();
    void print() const;
    void push_back(double value);
    double& operator[] (size_t index);
    size_t size() const {return size_;};
  private: 
    void increaseCapacity (size_t coefficient = 2);
    double* data_;
    size_t capacity_;
    size_t size_ = 0;
};

DynArray::DynArray(size_t capacity)
  : capacity_(capacity)
{
  data_ = new double[capacity_];
}
DynArray::~DynArray()
{
  delete data_;
}

void DynArray::print() const
{
  if(size_==0)
  {
    std::cout << "[empty]" << std::endl;
  }
  else
  {
    std::cout << "[";
    for(size_t i = 0; i<size_-1; ++i)
      std::cout << data_[i] << ", ";
    std::cout << data_[size_-1] << "]" << std::endl; 
  }
}

void DynArray::push_back(double value)
{
  if(size_ == capacity_)
    increaseCapacity();
  data_[size_] = value;
  size_++;
}

double& DynArray::operator[] (size_t index)
{
  if(index >= size_)
    throw "index out of bounds";
  else
    return data_[index];
}

void DynArray::increaseCapacity(size_t coef)
{
  std::cout << "Size increase" << std::endl;
  if(coef <= 1)
    throw "can't increase capacity with a factor less than 2";
  capacity_ = coef * capacity_;
  double* newData = new double[capacity_];
  for(size_t i = 0; i<size_; ++i)
  {
    newData[i] = data_[i];
  }
  delete[] data_;
  data_ = newData;
}

void merge(DynArray& arr, size_t left, size_t mid, size_t right, double* helper)
{
  for(size_t i = left; i <= right; ++i)
    helper[i] = arr[i];
  size_t iLeft = left;
  size_t iRight = mid+1;
  size_t index = left;
  while(iLeft <= mid && iRight <= right)
  {
    if(helper[iLeft] <= helper[iRight])
    {
      arr[index] = helper[iLeft];
      index++;
      iLeft++;
    }
    else
    {
      arr[index] = helper[iRight];
      index++;
      iRight++;
    }
  }
  while(iLeft <= mid)
  {
    arr[index] = helper[iLeft];
    index++;
    iLeft++;
  }
}

void mergeSort(DynArray& arr, size_t left, size_t right, double* helper)
{ 
  if(right > left)
  {
    int mid = (right+left)/2;
    mergeSort(arr, left, mid, helper);
    mergeSort(arr, mid+1, right, helper);
    merge(arr, left, mid, right, helper);
  }
}

void mergeSort(DynArray& arr)
{
  if(arr.size()==0)
    return;

  double* helper = new double[arr.size()];
  mergeSort(arr, 0, arr.size()-1,helper);
}

int main()
{
  try
  {
    DynArray a;
    a.push_back(34);
    a.print();
    a.push_back(24);
    a.push_back(42.3);
    a.push_back(142.3);
    a.push_back(-242.3);
    a.push_back(342.3);
    a.push_back(1);
    a.push_back(42.3);
    a.push_back(42.3);
    a.push_back(-12.54);
    a.push_back(0);
    a.print();
    a[3] = 10;
    a.print();
    mergeSort(a);
    a.print();

    DynArray sorted;
    sorted.push_back(0);
    sorted.push_back(1);
    sorted.push_back(2);
    sorted.push_back(3);
    sorted.push_back(4);
    sorted.push_back(5);
    sorted.push_back(6);
    sorted.print();
    mergeSort(sorted);
    sorted.print();

    DynArray reverseSorted;
    reverseSorted.push_back(5);
    reverseSorted.push_back(4);
    reverseSorted.push_back(3);
    reverseSorted.push_back(2);
    reverseSorted.push_back(1);
    reverseSorted.push_back(0);
    reverseSorted.print();
    mergeSort(reverseSorted);
    reverseSorted.print();

    DynArray emptyArray;
    emptyArray.print();
    mergeSort(emptyArray);
    emptyArray.print();
  }
  catch( const char* e)
  {
    std::cerr << e << std::endl;
  }
}



