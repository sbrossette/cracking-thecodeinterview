
#include <stdlib.h>
#include <iostream>
#include <string>
#include <algorithm>

class Node
{
  public:
    Node(const std::string& key, const std::string& value);
    virtual ~Node();
    void add(const std::string& key, const std::string& value);
    void print();
  private:
    Node* next_ = nullptr;
    std::string key_, value_;
};

Node::Node(const std::string& key, const std::string& value)
  : key_(key),
    value_(value)
{
}

Node::~Node() 
{
}

void Node::add(const std::string& key, const std::string& value)
{
  Node* tail = this;
  while(tail->next_ != nullptr)
  {
    tail = tail->next_;
  }
  Node* newNode = new Node(key, value);
  tail->next_ = newNode;
}

void Node::print()
{
  Node* currentNode = this;
  while(currentNode->next_ != nullptr)
  {
    std::cout << currentNode->key_ << ": " << currentNode->value_ << ", ";
    currentNode = currentNode->next_;
  }
  std::cout << currentNode->key_ << ": " << currentNode->value_ << ", ";
}

class HashTable
{
  public:
    HashTable(size_t size);
    virtual ~HashTable();
    void insert(const std::string& key, const std::string& value);
    void print();
    
  private:
    size_t computeHash(const std::string& s) const;
    Node** table_;
    size_t size_;
};

HashTable::HashTable(size_t size)
  :  size_(size)

{
  table_ = (Node**)malloc(size*sizeof(Node));
}

size_t HashTable::computeHash(const std::string& s) const
{
  std::hash<std::string> hasher;
  size_t res = hasher(s) % size_;
  return res;
}

HashTable::~HashTable()
{
  free(table_);
}
void HashTable::insert(const std::string& key, const std::string& value)
{
  //compute hash of key between 0 and size-1
  //insert pair in Node[hash(key)]
  size_t h = computeHash(key);
  std::cout << "h: " << h << std::endl;
  if(table_[h])
    table_[h]->add(key,value);
  else
    table_[h] = new Node(key,value);
}

void HashTable::print()
{
  for(size_t i = 0; i < size_; ++i)
  {
    std::cout << "Table[" << i << "]: ";
    if(table_[i])
      table_[i]->print();
    std::cout << std::endl;
  }
}

class DynArray
{
  public:
    DynArray(size_t capacity);
    virtual ~DynArray();
    void push_back(const std::string& s);
    void print();
  private:
    void increaseCapacity(size_t coeff=2);
    std::string* array_;
    size_t capacity_;
    size_t size_ = 0;
};

DynArray::DynArray(size_t capacity)
  :  capacity_(capacity)
{
  array_ = new std::string[capacity_];
}

DynArray::~DynArray()
{
  delete[] array_;
}

void DynArray::increaseCapacity(size_t coefficient)
{
    std::cout << "Increase Capacity!" << std::endl;   
    capacity_ *= coefficient;
    std::string* newArray = new std::string[capacity_];
    for(size_t i = 0; i<size_; ++i)
    {
      newArray[i] = array_[i];
    }
    delete[] array_;
    array_ = newArray;
}

void DynArray::push_back(const std::string& s)
{
  print();
  if(size_ >= capacity_)
    increaseCapacity();

  array_[size_] = s;
  size_++;
}

void DynArray::print()
{
  std::cout << "Size: " << size_ << ", " << "Capa: " << capacity_;
  if(size_ > 0)
  {
    std::cout << ", [";
    for(size_t i = 0; i < size_-1; ++i)
      std::cout << array_[i] << ", ";
    std::cout << array_[size_-1] << "]";
  }
  std::cout << std::endl;
}



int main()
{
  Node a("a", "Coucou");
  a.add("b", ", ");
  a.add("c", "ca va?");
  a.print();

  HashTable hashTable(10);
  hashTable.insert("car1", "Tesla");
  hashTable.insert("car2", "Volve");
  hashTable.insert("sport1", "Golf");
  hashTable.insert("sport2", "Tennis");
  hashTable.insert("game", "Zelda");
  hashTable.insert("city", "San Francisco");
  hashTable.insert("city2", "London");
  hashTable.insert("cloth", "dress");

  hashTable.print();

  DynArray vec(3);
  vec.push_back("q");
  vec.push_back("w");
  vec.push_back("e");
  vec.push_back("r");
  vec.push_back("t");
  vec.push_back("y");
  vec.push_back("u");
  vec.push_back("i");
  vec.push_back("o");
  vec.push_back("p");
  
  std::string s("HelloWorld");
  std::cout << "s: " << s << std::endl;
  std::cout << s[0] << std::endl;
  std::cout << s[2] << std::endl;
  std::cout << "s.size(): " << s.size() << std::endl;
  std::sort(s.begin(), s.end());
  std::cout << "s: " << s << std::endl;
}




















