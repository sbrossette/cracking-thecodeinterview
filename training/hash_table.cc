#include <iostream>
#include <unordered_map>
#include <set>
#include <string>
#include <utility>

int main(void)
{
  std::unordered_map<std::string, double> myMap;
  myMap.insert(std::make_pair<std::string,double>("Hello", 1));
  myMap.insert(std::make_pair<std::string,double>("my", 2));
  myMap.insert(std::make_pair<std::string,double>("Name", 3));
  myMap.insert(std::make_pair<std::string,double>("Stan", 4));
  myMap.insert(std::make_pair<std::string,double>("Hello", 4));
  std::cout << myMap["Hello"] << std::endl;
  std::set<int> mySet;
  mySet.insert(1);
  mySet.insert(1);
  mySet.insert(1);
  mySet.insert(1);
  std::cout << mySet.count(1)<< std::endl;
  return 0;
}
