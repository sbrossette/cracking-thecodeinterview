
#include <iostream>

template <typename T>
class Node
{
  public:
    Node(T data);
    virtual ~Node();
    void append(T data);
    void print();
    Node<T>* getTail();

    Node<T>* next_ = nullptr;
    T data_;
};

template<typename T>
Node<T>::Node(T data)
  : data_(data)
{}

template<typename T>
Node<T>::~Node()
{
  delete next_;
}

template<typename T>
void Node<T>::append(T data)
{
  Node<T>* n = this;
  while(n->next_ != nullptr)
    n = n->next_;
  Node<T>* newNode = new Node(data);
  n->next_ = newNode;
}

template<typename T>
void Node<T>::print()
{  
  Node<T>* n = this;
  while(n->next_ != nullptr)
  {
    std::cout << n->data_ << ", ";
    n = n->next_;
  }
  std::cout << n->data_ << std::endl;
}

template<typename T>
Node<T>* Node<T>::getTail()
{
  Node<T>* tail = this;
  while(tail->next_ != nullptr)
    tail = tail->next_;
  return tail;
}

template <typename T>
class Stack
{
  public:
    Stack(T value);
    virtual ~Stack();
    Node<T>* pop();
    void push(T value);
    void print();
    T peek();
    T min();
    bool isEmpty();
  private:
    Node<T>* top_;
    Node<T>* min_;
};

template<typename T>
Stack<T>::Stack(T value)
{
  top_ = new Node<T>(value);
  min_ = new Node<T>(value);
}

template<typename T>
Stack<T>::~Stack()
{
  delete top_;
  delete min_;
}

template<typename T>
Node<T>* Stack<T>::pop()
{ 
  Node<T>* res = nullptr;
  if(top_ != nullptr)
  {
    res = top_;
    if(min_->data_ == top_->data_)
      min_ = min_->next_;
    top_ = top_->next_;
  }
  return res;
}

template<typename T>
void Stack<T>::push(T data)
{
  Node<T>* newHead = new Node<T>(data);
  newHead->next_ = top_;
  top_ = newHead;
  if(data <= min_->data_)
  {
    Node<T>* newMin = new Node<T>(data);
    newMin->next_ = min_;
    min_ = newMin;
  } 
}

template<typename T>
T Stack<T>::peek()
{
  if(!isEmpty())
    return top_->data_;
  else
    throw "Cannot peek, stack empty!";
}

template<typename T>
void Stack<T>::print()
{
  top_->print();
  min_->print();
}

template<typename T>
T Stack<T>::min()
{
  return min_->data_;
}

template<typename T>
bool Stack<T>::isEmpty()
{
  return top_ == nullptr;
}

template <typename T>
class Queue
{
  public:
    Queue(T data);
    virtual ~Queue();
    void add(T data);
    Node<T>* remove();
    T peek();
    bool isEmpty();
    void print();
  private:
    Node<T>* first_ = nullptr;
    Node<T>* last_ = nullptr;
};

template <typename T>
Queue<T>::Queue(T data)
{
  first_ = new Node<T>(data);
  last_ = first_;
}
template <typename T>
Queue<T>::~Queue()
{}
template <typename T>
void Queue<T>::add(T data)
{
  last_->next_ = new Node<T>(data);
  last_ = last_->next_;
}
template <typename T>
Node<T>* Queue<T>::remove()
{
  if(!isEmpty())
  {
    Node<T>* tmp = first_;
    first_ = first_->next_;
    return tmp;
  }
  else
  {
    throw "Cannot remove from empty queue";
  }
}
template <typename T>
T Queue<T>::peek()
{
  if(!isEmpty())
    return first_;
  else
    throw "Cannot peek on empty queue";
}
template <typename T>
bool Queue<T>::isEmpty()
{
  return first_ == nullptr;
}
template <typename T>
void Queue<T>::print()
{
  first_->print();
}



int main(void)
{
  try
  {
    Stack<int> a(5);
    a.push(3);
    a.push(4);
    a.push(2);
    a.push(2);
    a.push(6);
    a.push(1);
    a.push(2);
    a.push(3);
    a.print();
    a.pop();
    a.print();
    a.pop();
    a.print();
    a.pop();
    a.print();
    a.pop();
    a.print();
    a.pop();
    a.print();
    a.pop();
    a.print();
    Queue<std::string> b("Hello");
    b.add(" my");
    b.add(" name");
    b.add(" is");
    b.add(" Stan!");
    b.print();
    b.remove();
    b.print();
    b.remove();
    b.print();
  }
  catch (const char* msg) 
  {
    std::cerr << msg << std::endl;
  }

  //Node<std::string> b("Hello");
  //b.append("World");
  //b.print();
  return 0;
}




