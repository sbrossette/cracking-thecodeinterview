
#include <iostream>

class Node
{
  public:
    Node(int data);
    virtual ~Node();
    void append(int data);
    void print();
    Node* partition(int value);
    Node* getTail();

    Node* next_ = nullptr;
    int data_;
};

Node::Node(int data)
  : data_(data)
{
}

Node::~Node()
{}

void Node::append(int data)
{
  Node* n = this;
  while(n->next_ != nullptr)
  {
    n = n->next_;
  }
  Node* newNode = new Node(data);
  n->next_ = newNode;
}

void Node::print()
{  
  Node* n = this;
  while(n->next_ != nullptr)
  {
    std::cout << n->data_ << ", ";
    n = n->next_;
  }
  std::cout << n->data_ << std::endl;
}

Node* Node::getTail()
{
  Node* tail = this;
  while(tail->next_ != nullptr)
    tail = tail->next_;
  return tail;
}

Node* Node::partition(int value)
{
  Node* smaller = nullptr;
  Node* bigger = nullptr;
  Node* smallerHead = nullptr;
  Node* biggerHead = nullptr;
  Node* n = this;
  while(n != nullptr)
  {
    if(n->data_ < value)
    {
      if(smaller == nullptr)
      {
        smaller = new Node(n->data_);
        smallerHead = smaller;
      }
      else
      {
        smaller->next_ = new Node(n->data_);
        smaller = smaller->next_;
      }
    }
    else
    {
      if(bigger == nullptr)
      {
        bigger = new Node(n->data_);
        biggerHead = bigger;
      }
      else
      {
        bigger->next_ = new Node(n->data_);
        bigger = bigger->next_;
      }
    }
    n = n->next_;
  }
  //Combine the two lists
  Node* result = nullptr;
  if(smallerHead != nullptr)
  {
    result = smallerHead;
    smaller->next_ = biggerHead;
  }
  else
  {
    result = biggerHead;
  }
  return result;
}

Node* sumDigits(Node* d1, Node* d2, int ret = 0)
{
  int sum = ret;
  Node* d1Next = nullptr;
  Node* d2Next = nullptr;
  if(d1)
  {
    sum = sum + d1->data_;
    d1Next = d1->next_;
  }
  if(d2)
  {
    sum = sum + d2->data_;
    d2Next = d2->next_;
  }

  Node* res = new Node(sum % 10);
  ret = (sum - res->data_)/10;
  if(d1Next != nullptr || d2Next != nullptr || ret != 0)
    res->next_ = sumDigits(d1Next, d2Next, ret);
  return res;
}

int main()
{
  Node n(0);
  n.append(12);
  n.append(2);
  n.append(3);
  n.append(4);
  n.append(5);
  n.append(4);
  n.append(8);
  n.append(4);
  n.append(4);
  n.print();
  Node* partitionnedN = n.partition(5);
  partitionnedN->print();

  Node* d1 = new Node(2);
  d1->append(1);
  d1->append(9);
  d1->append(4);
  
  Node* d2 = new Node(9);
  d2->append(7);
  d2->append(2);

  Node* sum = sumDigits(d1,d2);
  d1->print();
  d2->print();
  sum->print();

  return 0;
}







