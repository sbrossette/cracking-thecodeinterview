#include <iostream>

class foo
{
public:
  foo (){}
  virtual ~foo (){}

private:
  
};

int main(void)
{
  foo f;
  int* a;
  int p = 12;
  int b[10];
  //a = new int[10];
  a = &(b[0]);
  b[4] = 24;
  std::cout << "a: " << a << std::endl;
  std::cout << "a[4]: " << a[4] << std::endl;
  std::cout << "&a[4]: " << &a[4] << std::endl;
  a = &p;
  std::cout << "a: " << a << std::endl;
  std::cout << "*a: " << *a << std::endl;
  std::cout << "a[1]: " << a[10000] << std::endl;
  return 0;
}
