
#include <iostream>
#include <vector>


struct DepthAndBalanced
{
  size_t depth_ = 0;
  bool isBalanced_ = true;
};

class Node 
{
  public:
    Node (int data);
    virtual ~Node();
    void addChild(int data);
    void preOrderTraversal();
    void inOrderTraversal();
    void postOrderTraversal();
    bool isBalanced();
   
  private:
    DepthAndBalanced getDepthAndBalanced();
    int data_;
    Node* left_ = nullptr;
    Node* right_ = nullptr;
};

Node::Node(int data)
  : data_(data)
{
}

Node::~Node()
{
  delete left_;
  delete right_;
}

void Node::addChild(int data)
{
  if(data < data_)
  {
    if(left_ == nullptr)
      left_ = new Node(data);
    else
      left_->addChild(data);
  }
  else if(data > data_)
  {
    if(right_ == nullptr)
      right_ = new Node(data);
    else
      right_->addChild(data);
  }
  else
    throw "Cannot add node with duplicate value";
}

void Node::preOrderTraversal()
{
  std::cout << data_ << std::endl;
  if(left_)
    left_->preOrderTraversal();
  if(right_)
    right_->preOrderTraversal();
}

void Node::inOrderTraversal()
{
  if(left_)
    left_->inOrderTraversal();
  std::cout << data_ << std::endl;
  if(right_)
    right_->inOrderTraversal();
}

void Node::postOrderTraversal()
{
  if(left_)
    left_->postOrderTraversal();
  if(right_)
    right_->postOrderTraversal();
  std::cout << data_ << std::endl;
}

bool Node::isBalanced()
{
  DepthAndBalanced res = getDepthAndBalanced();
  std::cout << "Depth: " << res.depth_ << std::endl;
  std::cout << "isBalanced: " << res.isBalanced_ << std::endl;
  return getDepthAndBalanced().isBalanced_;
}

DepthAndBalanced Node::getDepthAndBalanced()
{
  DepthAndBalanced result, resLeft, resRight;
  if(left_)
    resLeft = left_->getDepthAndBalanced();
  if(right_)
    resRight = right_->getDepthAndBalanced();

  size_t diffDepth;

  if(resLeft.depth_ > resRight.depth_)
  {
    result.depth_ = resLeft.depth_+1;
    diffDepth = resLeft.depth_ - resRight.depth_;
  }
  else
  {
    result.depth_ = resRight.depth_+1;
    diffDepth = resRight.depth_ - resLeft.depth_;
  }
  result.isBalanced_ = resLeft.isBalanced_ && resRight.isBalanced_ && diffDepth <= 1;
  return result;
}



class MinHeap
{
  public:
    MinHeap();
    ~MinHeap();
    size_t parent(size_t i);
    size_t left(size_t i);
    size_t right(size_t i);
    void add(int value);
    int extractMin();
    void print();
    
  private:
    void swap(size_t i1, size_t i2);
    std::vector<int> data_;
    size_t size_ = 0;
};

MinHeap::MinHeap()
{
}
MinHeap::~MinHeap()
{
}
size_t MinHeap::parent(size_t i)
{
  if(i == 0)
    throw "Root does not have parents";
  else
    return (i-1)/2;
}
size_t MinHeap::left(size_t i)
{
  return 2*i+1;
}
size_t MinHeap::right(size_t i)
{
  return 2*i+2;
}
void MinHeap::add(int value)
{
  data_.push_back(value);
  size_++;
  int indexVal = size_-1;
  while(indexVal != 0 && data_[parent(indexVal)]>value)
  {
    swap(indexVal, parent(indexVal));
    indexVal = parent(indexVal);
  }
}
int MinHeap::extractMin()
{
  if(size_==0)
    throw "Cannot extract min of empty heap";
  else
  {
    int min = data_[0];
    swap(0,size_-1);
    size_--;
    size_t current = 0;
    bool done = false;
    while(!done)
    {
      size_t smallestChild = left(current);
      size_t biggestChild = right(current);

      if(right(current) < size_)
      {
        if(data_[right(current)] < data_[left(current)])
        {
          smallestChild = right(current);
          biggestChild = left(current);
        }
      }
      if(smallestChild < size_ && data_[smallestChild] < data_[current])
      {
        swap(smallestChild,current);
        current = smallestChild;
      }
      else if(biggestChild < size_ && data_[biggestChild] < data_[current])
      {
        swap(biggestChild,current);
        current = biggestChild;
      }
      else
        done = true;
    }
    return min;
  }
}
void MinHeap::swap(size_t i1, size_t i2)
{
  int tmp = data_[i1];
  data_[i1] = data_[i2];
  data_[i2] = tmp;
}
void MinHeap::print()
{
  size_t nextRowBeginIndex = 1;
  size_t index = 0;
  while(index < size_)
  {
    while(index < nextRowBeginIndex && index < size_)
    {
      std::cout << data_[index] << ", ";
      index++;
    }
    nextRowBeginIndex = 2*(nextRowBeginIndex+1)-1;
    std::cout << std::endl;
  }
}


int main()
{
try
{ 
  Node b(10);
  b.addChild(5);
  b.addChild(15);
  b.addChild(1);
  b.addChild(7);
  b.addChild(12);
  b.inOrderTraversal();
  b.isBalanced();

  MinHeap a;
  a.add(14);
  a.add(15);
  a.add(130);
  a.add(12);
  a.add(3);
  a.add(20);
  a.add(18);
  a.add(120);
  a.add(8);
  a.print();
  std::cout<<"Min: "<<a.extractMin()<<std::endl;
  a.print();
}
catch (const char* msg)
{
  std::cerr << msg << std::endl;
}

}






