#pragma once
#include <vector>
#include "utils.hh"

std::vector<double> bubbleSort(const std::vector<double>& vecOriginal)
{
  std::vector<double> vec = vecOriginal;
  bool sorted = false;
  while (!sorted)
  {
    sorted = true;
    for (size_t i = 0; i < vec.size() - 1; i++)
    {
      if (vec.at(i) > vec.at(i + 1))
      {
        sorted = false;
        swap(vec[i], vec[i + 1]);
      }
    }
  }
  return vec;
}
