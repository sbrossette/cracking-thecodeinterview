#pragma once
#include <iostream>
#include <vector>
#include <limits>

#include "utils.hh"

std::vector<double> selectionSort(const std::vector<double>& vecOriginal)
{
  std::vector<double> vec = vecOriginal;
  for (size_t i = 0; i < vec.size(); i++)
  {
    double currentMin = std::numeric_limits<double>::max();
    size_t currentMinIndex = -1;
    for (size_t j = i; j < vec.size(); j++)
    {
      if (vec[j] < currentMin)
      {
        currentMin = vec[j];
        currentMinIndex = j;
      }
    }
    swap(vec[i],vec[currentMinIndex]);
  }
  return vec;
}

