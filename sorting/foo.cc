#include <ctime>
#include <stdlib.h>
#include <vector>
#include <algorithm>
 

#include "selectionSort.hh"
#include "mergeSort.hh"
#include "quickSort.hh"
#include "bubbleSort.hh"

std::vector<double> randomVec(int size, double maxVal)
{
  std::vector<double> x(size, 0.0);
  for (int i = 0; i < x.size(); i++)
  {
    x.at(i) = (rand() / (double)RAND_MAX) * maxVal;
  }
  return x;
}

void printDuration(const std::clock_t start)
{
  double d = (std::clock() - start) / (double)CLOCKS_PER_SEC;
  std::cout << "took " << d << " seconds" << std::endl;
}

int main(int argc, char *argv[])
{
  int size = 100;
  if(argc>=2)
    size = atoi(argv[1]);

  std::clock_t start;
  std::vector<double> v = randomVec(size,100);
  std::vector<double> vResBubble(size);
  std::vector<double> vResSelection(size);
  std::vector<double> vResMerge(size);
  std::vector<double> vResQuick(size);
  //std::vector<double> v = { 1, 2, 3, 5, 32, 23, 4, 6, 4, 32, 63, 7, 42, 36, 326, 74, 73, 2437, 38, 4, 3, 27, 74 };

  std::cout << "Selection sort:" << std::endl;
  //print(v);
  start = std::clock();
  vResSelection = selectionSort(v);
  //print(selectionSort(v));
  printDuration(start);

  std::cout << "Bubble sort:" << std::endl;
  //print(v);
  start = std::clock();
  vResBubble = bubbleSort(v);
  //print(bubbleSort(v));
  printDuration(start);

  std::cout << "Merge sort:" << std::endl;
  //print(v);
  start = std::clock();
  vResMerge = mergeSort(v);
  //print(mergeSort(v));
  printDuration(start);

  std::cout << "Quick sort:" << std::endl;
  //print(v);
  start = std::clock();
  vResQuick = quickSort(v);
  //print(quickSort(v));
  printDuration(start);

  std::cout << "v: " << isSorted(v) << std::endl;
  std::cout << "BubbleSort: " << isSorted(vResBubble) << std::endl;
  std::cout << "SelectionSort: " << isSorted(vResSelection) << std::endl;
  std::cout << "MergeSort: " << isSorted(vResMerge) << std::endl;
  std::cout << "QuickSort: " << isSorted(vResQuick) << std::endl;

  return 0;
}
