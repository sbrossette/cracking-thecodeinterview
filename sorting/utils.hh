#pragma once
#include <iostream>
#include <vector>

void print(std::vector<double> v)
{
  std::cout << v.at(0);
  for (size_t i = 1; i < v.size(); i++) 
  {
    std::cout << ", " <<  v.at(i);
  }
  std::cout << std::endl;
}

void swap(double& d1, double& d2)
{
  double tmp = d1;
  d1 = d2;
  d2 = tmp;
}

void printSegment(const std::vector<double>& vec, size_t left, size_t right)
{
  std::cout << "[" << vec.at(left);
  for (size_t i = left + 1; i <= right; i++) std::cout << ", " << vec.at(i);
  std::cout << "]";
}

void swap(std::vector<double>& vec, size_t left, size_t right)
{
  double tmp = vec[left];
  vec[left] = vec[right];
  vec[right] = tmp;
  return;
}

bool isSorted(const std::vector<double>& vec)
{
  for (size_t i = 0; i < vec.size() - 1; i++)
  {
    if (vec.at(i) > vec.at(i + 1)) return false;
  }
  return true;
}
