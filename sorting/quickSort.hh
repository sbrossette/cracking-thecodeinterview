#include <iostream>
#include <vector>
#include "utils.hh"

void quickSort(std::vector<double>& vec, int left, int right);
int partition(std::vector<double>& vec, int left, int right);
std::vector<double> quickSort(const std::vector<double> vecOriginal);


std::vector<double> quickSort(const std::vector<double> vecOriginal)
{
  std::vector<double> vec = vecOriginal;
  quickSort(vec, 0, vec.size() - 1);
  return vec;
}
void quickSort(std::vector<double>& vec, int left, int right)
{
  int index = partition(vec, left, right);
  if (left < index - 1) quickSort(vec, left, index - 1);
  if (index < right) quickSort(vec, index, right);
}

int partition(std::vector<double>& vec, int left, int right)
{
  double pivot = vec[(left+right)/2];
  while(left<=right)
  {
    while (vec[left] < pivot) left++;
    while (vec[right] > pivot) right--;
    if (left <= right)
    {
      swap(vec, left, right);
      left++;
      right--;
    }
  }
  return left;
}
