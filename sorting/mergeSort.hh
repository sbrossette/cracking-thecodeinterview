#pragma once
#include <iostream>
#include <vector>
#include "utils.hh"

void merge(std::vector<double>& vec, std::vector<double>& helper, size_t left, size_t mid, size_t right);
void mergeSort(std::vector<double>& vec, std::vector<double>& helper, size_t left, size_t right);

std::vector<double> mergeSort(const std::vector<double> vecOriginal)
{
  std::vector<double> vec = vecOriginal;
  std::vector<double> helper(vec.size());
  mergeSort(vec, helper, 0, vec.size() - 1);
  return vec;
}

void mergeSort(std::vector<double>& vec, std::vector<double>& helper,
               size_t left, size_t right)
{
  if(left<right)
  {
    size_t mid = (left + right) / 2;
    mergeSort(vec, helper, left, mid);
    mergeSort(vec, helper, mid + 1, right);
    merge(vec, helper, left, mid, right);
  }
}

void merge(std::vector<double>& vec, std::vector<double>& helper, size_t left,
           size_t mid, size_t right)
{
  for (size_t i = left; i <= right; i++) helper[i] = vec[i];

  size_t helperLeft = left;
  size_t helperRight = mid + 1;
  size_t current = left;

  while (helperLeft <= mid && helperRight <= right)
  {
    if (helper[helperLeft] <= helper[helperRight])
    {
      vec[current] = helper[helperLeft];
      helperLeft++;
    }
    else
    {
      vec[current] = helper[helperRight];
      helperRight++;
    }
    current++;
  }

  int remaining = mid - helperLeft;
  for (int i = 0; i <= remaining; i++)
    vec[current + i] = helper[helperLeft + i];
}
