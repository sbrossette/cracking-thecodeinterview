#include <iostream>
#include <vector>
#include <queue>


template <typename T>
class Node
{
public:
 Node(const T& item) : data_(item) {}
 virtual ~Node() {}
 void addAdjacent(const T& item)
 {
   adjacents_.push_back(Node(item));
 }
 void print() const
 {
   std::cout << data_ << std::endl;
 }

public:
 T data_;
 std::vector<Node<T>> adjacents_;
};

template <typename T>
class Graph
{
  std::vector<Node<T> > nodes_;
};

template <typename T>
void DFS(const Node<T>& n)
{
  n.print();
  for (auto adj : n.adjacents_) DFS(adj);
}

template <typename T>
void BFS(const Node<T>& n)
{
  std::queue<Node<T>> q;
  q.push(n);
  while(!q.empty())
  {
    q.front().print();
    for(auto n:q.front().adjacents_)
      q.push(n);
    q.pop();
  }
}
