#include "../LinkedList.hh"

int main(void)
{
  LinkedList<int> a(1);
  a.appendToTail(2);
  a.appendToTail(3);
  a.appendToTail(4);
  a.appendToTail(3);
  a.appendToTail(2);
  a.print();
  a.deleteNode(2);
  a.print();
  a.deleteNode(1);
  a.print();
  return 0;
}
