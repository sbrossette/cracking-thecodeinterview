#include "../HashTable.hh"

int main(void)
{
  HashTable myHash;
  myHash.add("Tesla", "Car");
  myHash.add("Apple", "iPhone");
  myHash.add("Walmart", "stuff");
  myHash.add("Walmart", "other stuff");
  myHash.add("Microsoft", "Windows");
  myHash.add("Linux", "OS");
  std::cout << myHash << std::endl;
  std::cout << "myHash[\"Tesla\"]: " << myHash["Tesla"] << std::endl;
  std::cout << "myHash[\"Test\"]: " << myHash[""] << std::endl;
  std::cout << "myHash[\"Walmart\"]: " << myHash["Walmart"] << std::endl;
  std::cout << "myHash[\"Apple\"]: " << myHash["Apple"] << std::endl;
  std::cout << "myHash[\"Apple\"]: " << myHash["Apple"] << std::endl;
  std::cout << "myHash[\"Linux\"]: " << myHash["Linux"] << std::endl;
  return 0;
}
