#include "../Stack.hh"

int main(void)
{
  Stack<int> s;
  s.push(1);
  s.push(2);
  s.print();
  std::cout << "s.peek().data_: " << s.peek().data_ << std::endl;

  s.push(3);
  s.push(4);
  s.push(7);
  s.push(9);
  s.print();
  std::cout << "s.peek().data_: " << s.peek().data_ << std::endl;

  s.pop();
  s.pop();
  s.pop();
  s.print();
  std::cout << "s.peek().data_: " << s.peek().data_ << std::endl;
  
  return 0;
}
