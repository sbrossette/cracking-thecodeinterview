#include "../Graph.hh"

int main(void)
{
  Node<int> root(-1);
  root.addAdjacent(1);
  root.addAdjacent(2);
  root.addAdjacent(3);
  root.addAdjacent(4);
  root.adjacents_[0].addAdjacent(10);
  root.adjacents_[0].addAdjacent(11);
  root.adjacents_[1].addAdjacent(20);
  root.adjacents_[1].addAdjacent(21);
  root.adjacents_[2].addAdjacent(30);
  root.adjacents_[2].addAdjacent(31);
  root.adjacents_[3].addAdjacent(40);
  root.adjacents_[3].addAdjacent(41);
  std::cout << "DFS:" << std::endl;
  DFS(root);
  std::cout << "BFS:" << std::endl;
  BFS(root);
  return 0;
}
