#include "../Queue.hh"

int main(void)
{
  Queue<int> q;
  q.add(1);
  q.add(2);
  q.print();
  std::cout << "q.peek().data_: " << q.peek().data_ << std::endl;

  q.remove();
  q.add(3);
  q.add(4);
  q.add(5);
  q.add(6);
  q.print();
  std::cout << "q.peek().data_: " << q.peek().data_ << std::endl;

  q.remove();
  q.remove();
  q.remove();
  q.print();
  std::cout << "q.peek().data_: " << q.peek().data_ << std::endl;
  
  return 0;
}

