#pragma once

#include <iostream>

template <typename T>
class Node
{
 public:
  Node(const T& d)
  {
    data_ = d;
    next_ = nullptr;
  }
  virtual ~Node() {}

  void appendToTail(const T& d)
  {
    Node* n = this;
    while (n->next_ != nullptr) n = n->next_;
    n->next_ = new Node(d);
  }

  int length()
  {
    int l = 1;
    Node<T>* n = this;
    while (n->next_ != nullptr)
    {
      n = n->next_;
      l++;
    }
    return l;
  }
  void print()
  {
    Node* n = this;
    std::cout << data_ << " ";
    while (n->next_ != nullptr)
    {
      n = n->next_;
      std::cout << n->data_ << " ";
    }
    std::cout << std::endl;
  }
  Node* deleteNode(const double& d)
  {
    if(data_ == d)
      return next_;
    
    Node* n = this;
    Node* prevN=this;
    while(n->next_ != nullptr)
    {
      prevN = n;
      n = n->next_;
      if(n->data_ == d)
      {
        prevN->next_ = n->next_;
        delete n;
      }
    }
    return this;
  }

  T data_;
  Node* next_;
};

template<typename T>
class LinkedList
{
  public:
    LinkedList()
    {
      head_ = nullptr;
    }
    LinkedList(const T& d)
    {
      head_ = new Node<T>(d);
    }
    Node<T>* head()
    {
      return head_;
    }
    void setHead(Node<T>* n)
    {
      head_ = n;
    }
    void appendToTail(const T&d)
    {
      if(head_ == nullptr)
        head_ = new Node<T>(d);
      head_->appendToTail(d);
    }
    void print()
    {
      head_->print();
    }
    void deleteNode(const T& d)
    {
      head_ = head_->deleteNode(d);
    }
  private:
    Node<T>* head_;
};
