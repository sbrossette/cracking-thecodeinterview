#include <iostream>

template<typename T>
class StackNode
{
public:
  StackNode (const T& item)
  {
    data_ = item;
  }
  virtual ~StackNode ()
  {
  }

  void print()
  {
    StackNode<T>* n = this;
    while(n != nullptr)
    {
      std::cout << n->data_ << std::endl;
      n = n->next_;
    }
  }

  T data_;
  StackNode<T>* next_ = nullptr;
};

template<typename T>
class Stack
{
public:
  Stack ()
  {
  }
  Stack (const T& item)
  {
    head_ = new StackNode<T>(item);
  }
  virtual ~Stack ()
  {
  }
  void push(const T& item)
  {
    StackNode<T>* n = new StackNode<T>(item);
    n->next_ = head_;
    head_ = n;
  }
  T pop()
  {
    if(head_ == nullptr)
    {
      throw "stack is already empty, can't pop";
      return 0;
    }
    StackNode<T>* prevHead = head_;
    head_ = head_->next_;
    return prevHead->data_;
  }
  T peek()
  {
    return head_->data_;
  }
  bool isEmpty()
  {
    return head_ == nullptr;
  }

  void print()
  {
    StackNode<int>* n = head_;
    while(n!=nullptr)
    {
      std::cout << n->data_ << std::endl;
      n = n->next_;
    }
    std::cout << std::endl;
  }

  StackNode<T>* head_ = nullptr;
};
