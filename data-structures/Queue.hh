#include <iostream>

template <typename T>
class QueueNode
{
 public:
  QueueNode(const T& item) { data_ = item; }
  virtual ~QueueNode() {}

  T data_;
  QueueNode<T>* next_ = nullptr;
};

template <typename T>
class Queue
{
 public:
  Queue() {}

  Queue(const T& item)
  {
    first_ = new QueueNode<int>(item);
    last_ = first_;
  }
  virtual ~Queue() {}
  void add(const T& item)
  {
    if (last_ == nullptr && first_ == nullptr)
    {
      first_ = new QueueNode<int>(item);
      last_ = first_;
      return;
    }
    last_->next_ = new QueueNode<T>(item);
    last_ = last_->next_;
  }
  void remove()
  {
    if (first_ == nullptr) throw "Remove on empty list";
    QueueNode<T>* oldFirst = first_;
    first_ = first_->next_;
    delete oldFirst;
  }
  QueueNode<T> peek() { return *first_; }
  bool isEmpty() { return first_ == nullptr; }
  void print()
  {
    QueueNode<T>* n = first_;
    while (n != nullptr)
    {
      std::cout << n->data_ << std::endl;
      n = n->next_;
    }
  }

 public:
  QueueNode<T>* first_ = nullptr;
  QueueNode<T>* last_ = nullptr;
};
