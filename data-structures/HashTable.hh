#pragma once
#include <iostream>
#include <string>
#include <list>
#include <array>
#include <functional>

class HashEntry
{
 public:
  HashEntry(const std::string& key, const std::string& value)
      : key_(key), value_(value)
  {
  }
  virtual ~HashEntry() {}
  const std::string& key() const { return key_; }
  const std::string& value() const { return value_; }
  void setValue(const std::string& val) { value_ = val; }

 private:
  std::string key_, value_;
};

class HashTable
{
 public:
  HashTable();
  void add(const std::string& key, const std::string& value);
  std::string operator[](const std::string& key);
  virtual ~HashTable();
  std::ostream& print(std::ostream& o) const;

 private:
  size_t computeIndex(const std::string& key) const;
  std::array<std::list<HashEntry>, 128> table_;
  std::hash<std::string> hash_fn_;
};

std::ostream& operator<<(std::ostream& o, const HashTable& f)
{
  return f.print(o);
}

HashTable::HashTable() {}

HashTable::~HashTable() {}

size_t HashTable::computeIndex(const std::string& key) const
{
  return hash_fn_(key) % table_.size();
}

void HashTable::add(const std::string& key, const std::string& value)
{
  size_t index(computeIndex(key));
  auto it = table_[index].begin();
  while (it != table_[index].end())
  {
    if (key.compare(it->key()) == 0)
    {
      it->setValue(value);
      return;
    }
    ++it;
  }
  table_[index].push_back(HashEntry(key, value));
}

std::string HashTable::operator[](const std::string& key)
{
  size_t index(computeIndex(key));
  auto it = table_[index].begin();
  while (it != table_[index].end())
  {
    if (key.compare(it->key()) == 0)
    {
      return it->value();
    }
    ++it;
  }
  return std::string("Key not found");
}

std::ostream& HashTable::print(std::ostream& o) const
{
  for (size_t i = 0; i < table_.size(); i++)
  {
    o << "table[" << i << "]: ";
    auto it = table_[i].begin();
    while (it != table_[i].end())
    {
      o << it->key() << " " << it->value() << " ";
      ++it;
    }
    o << std::endl;
  }
  return o;
}

