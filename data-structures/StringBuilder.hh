#pragma once

#include <iostream>
#include <string>
#include <list>

class StringBuilder
{
 public:
  StringBuilder();
  virtual ~StringBuilder();
  void append(const std::string& element);
  std::string toString();

 private:
  std::list<std::string> data_;
};

StringBuilder::StringBuilder() {}
StringBuilder::~StringBuilder() {}

void StringBuilder::append(const std::string& element)
{
  data_.push_back(element);
}

std::string StringBuilder::toString()
{
  std::string res;
  for (std::list<std::string>::const_iterator it = data_.begin();
       it != data_.end(); it++)
    res.append(*it);
  return res;
}
