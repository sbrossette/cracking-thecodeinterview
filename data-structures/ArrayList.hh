#pragma once

#include <iostream>
#include <array>
#include <cstring>

template <typename T>
class ArrayList
{
 public:
  ArrayList(size_t capacity = 8);
  virtual ~ArrayList();
  void push_back(const T& element);
  void print();
  void removeLast() { size_--; };
  T& getLast() { return data_[size_ - 1]; }
  T& get(size_t i) { return data_[i]; }
  size_t size() const {return size_;}

 private:
  T* data_;
  size_t capacity_, size_;
};

template <typename T>
ArrayList<T>::ArrayList(size_t cap)
    : capacity_(cap), size_(0)
{
  data_ = new T[capacity_];
}

template <typename T>
ArrayList<T>::~ArrayList()
{
  delete[] data_;
}

template <typename T>
void ArrayList<T>::push_back(const T& element)
{
  if (size_ + 1 > capacity_)
  {
    // double size
    std::cout << "doubling capacity" << std::endl;
    capacity_ *= 2;
    T* newData = new T[capacity_];
    std::memcpy(newData, data_, sizeof(T) * size_);
    delete[] data_;
    data_ = newData;
  }
  data_[size_] = element;
  size_++;
}

template <typename T>
void ArrayList<T>::print()
{
  for (size_t i = 0; i < size_; i++)
  {
    std::cout << data_[i] << ", ";
  }
  std::cout << std::endl;
}

